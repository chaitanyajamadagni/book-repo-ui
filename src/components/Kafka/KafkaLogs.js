import React from "react";
import { Table } from "react-bootstrap";

export default class KafkaLogs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logs: []
    };
  }

  componentDidMount() {
    console.log("componentDidMount :: ", this.props.data);
    this.setState({ logs: this.props.data });
  }

  render() {
    console.log("In Render :: ", this.state.logs);
    var id = 0;
    var displayString = "";
    if (this.props.data != null) {
      displayString = this.state.logs.map((item, i) => {
        var str = item.split("-->");
        console.log("logs string : ", str);
        return (
          <tr key={i}>
            <td>{str[0]}</td>
            <td>{str[1]}</td>
            <td>{str[2]}</td>
            <td>{str[3] != null ? str[3] : "NA"}</td>
            <td>$ {str[4] != null ? str[4] : "NA"}</td>
            <td>{str[5] != null ? str[5] : "NA"}</td>
          </tr>
        );
      });
    }

    return (
      <div class="kafka-container">
        <h4>Kafka Logs</h4>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Operation</th>
              <th>ID</th>
              <th>Title</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Timestamp</th>
            </tr>
          </thead>
          <tbody>{displayString}</tbody>
        </Table>
      </div>
    );
  }
}
