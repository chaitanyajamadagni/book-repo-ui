import React from 'react';
import BookCards from './BookCards';
import {shallow, mount} from 'enzyme';


it('renders BookCards without crashing', () => {

  let i = 0;
  let book = {
    id:1111,
    title: "FakeTitle",
    description: "FakeDescription",
    price: 0.0,
    quantity:0,
    authors : ["FakeAuthor1"],
    image : "fakeImageUrl"
    }
  };
  const showSuccess = jest.fn();
  shallow(<BookCards key={i} data={book}/>);
});
