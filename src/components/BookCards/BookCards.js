import React, { Component } from "react";
import { Card, Button, Col, Row, Container, Modal, Img } from "react-bootstrap";
import axios from "axios";
import "./BookCards.css";

export default class BookCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deleted: false,
      open: false,
      show: false,
      updated: false,
      showAddButtonOnPopup: false
    };
    this.delete = this.delete.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
    this.addBooks = this.addBooks.bind(this);
    this.handleShowAddBook = this.handleShowAddBook.bind(this);
  }

  componentWillMount() {
    console.log("this is props data", this.props.data);
  }

  delete(e) {
    console.log("ref id is : ", this.props.data.id); //getting the value of paragraphs textContent
    //var id = this.id.textContent;
    var id = this.props.data.id;
    var url = `http://localhost:8080/books/${id}`;
    axios.delete(url).then(response => {
      // this.setState({deleted: true})
      this.props.deleted();
    });
  }

  saveChanges() {
    console.log("here in save boook-------");
    console.log(this.image.src);
    console.log(this.title.textContent);
    console.log(this.author.textContent);
    console.log(this.price.value);
    console.log(this.quantity.value);
    console.log(this.props.data.description);
    console.log(this.props.data.id);

    var book = {
      id: this.props.data.id,
      title: this.title.textContent,
      imageLinks: {
        thumbnail: this.image.src
      },
      authors: [this.author.textContent],
      price: parseFloat(this.price.value),
      quantity: parseInt(this.quantity.value),
      description: this.props.data.description
    };

    let config = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    var id = this.props.data.id;
    // console.log("id is :",id);
    console.log("book data is as : ", book);
    var url = `http://localhost:8080/books/${id}`;
    axios.put(url, book, config).then(response => {
      console.log("data for edit", response);
    });
    this.setState({ show: false });
    this.props.updated("updated");
  }

  addBooks() {
    var min = 1;
    var max = 10000;

    let config = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    let params = new FormData();
    params.append("id", Math.floor(Math.random() * (+max - +min)) + +min);
    params.append("title", this.title.textContent);
    params.append("authors", [this.author.textContent]);
    params.append("price", parseFloat(this.price.value));
    params.append("quantity", parseInt(this.quantity.value));
    params.append("description", this.props.data.description);
    params.append("imageLinks", this.image.src);

    axios.post("http://localhost:8080/books", params, config).then(response => {
      console.log(response.data);
    });

    this.setState({ show: false });
    // this.props.updated("updated");
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    console.log("here");
    this.setState({ show: true });
  }

  handleShowAddBook() {
    console.log("here");
    this.setState({ show: true, showAddButtonOnPopup: true });
  }

  render() {
    // console.log("data from props ->>>>>>>",this.props.data);
    var buttonString = "";
    if (this.state.showAddButtonOnPopup == true) {
      buttonString = (
        <Button variant="primary" onClick={this.addBooks}>
          Add Books
        </Button>
      );
    } else {
      buttonString = (
        <Button variant="primary" onClick={this.saveChanges}>
          save
        </Button>
      );
    }

    var text = "";
    if (this.state.show == true) {
      console.log("here2");
      var displayModalPopup = (
        <div>
          <Modal
            size="lg"
            aria-labelledby="example-modal-sizes-title-lg"
            show={this.state.show}
            onHide={this.handleClose}
          >
            <Modal.Header closeButton>
              <Modal.Title>Enter Price and Quantity</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container>
                <Row>
                  <Col>
                    <div className="leftDiv">
                      <img
                        variant="top"
                        src={this.props.data.imageLinks}
                        height="200"
                        width="200"
                        ref={node => (this.image = node)}
                      />
                    </div>
                  </Col>
                  <Col>
                    <div className="rightDiv">
                      <br />
                      <Row>
                        <h4 ref={node => (this.title = node)}>
                          {" "}
                          {this.props.data.title}{" "}
                        </h4>
                      </Row>
                      <Row>
                        <h4> by &nbsp;</h4>{" "}
                        <h4 ref={node => (this.author = node)}>
                          {" "}
                          {this.props.data.author}
                        </h4>{" "}
                      </Row>
                      <Row>
                        <input
                          type="text"
                          placeholder="Price"
                          size="50"
                          ref={node => (this.price = node)}
                        />{" "}
                      </Row>
                      <br />
                      <Row>
                        <input
                          type="text"
                          placeholder="Available Quantity"
                          size="50"
                          ref={node => (this.quantity = node)}
                        />{" "}
                      </Row>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <h5> Description: </h5>
                  {this.props.data.description}
                </Row>
              </Container>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleClose}>
                Close
              </Button>
              {buttonString}
            </Modal.Footer>
          </Modal>
        </div>
      );
    }

    var showHideButtons = "";
    if (this.props.disableButtons == false) {
      showHideButtons = (
        /*  <Container>
          <Row>
            <Col>
              <Button variant="link" onClick={this.delete}>
                Delete
              </Button>
            </Col>
            <Col>
              <Button variant="link" onClick={this.handleShow}>
                Edit
              </Button>
            </Col>
          </Row>
        </Container>*/

        <table>
          <tr>
            <td>
              <Button variant="link" onClick={this.delete}>
                Delete
              </Button>
            </td>
            <Button variant="link" onClick={this.handleShow}>
              Edit
            </Button>
          </tr>
        </table>
      );
    } else {
      showHideButtons = (
        <Container>
          <Row>
            <Col>
              <Button variant="primary" onClick={this.handleShowAddBook}>
                Add Book
              </Button>
            </Col>
          </Row>
        </Container>
      );
    }

    return (
      <div className="card-body">
        <table className="table-body">
          <tr>
            <td>
              <img
                className="card-img-top"
                src={this.props.data.imageLinks}
                alt="Card image"
                style={{ width: 120, height: 160 }}
              />
            </td>
            <td style={{ paddingLeft: 20 }}>
              <span className="card-title">
                <strong>{this.props.data.title}</strong>
              </span>
              <br />
              <span className="card-text">{this.props.data.author}</span>
              <br />
              <br />
              <span className="card-text">
                {this.props.data.price
                  ? "Price : $ " + this.props.data.price
                  : ""}
              </span>
              <br />
              <span className="card-text">
                {this.props.data.quantity
                  ? this.props.data.quantity + " copies avaiable"
                  : ""}
              </span>
              <br />
              {showHideButtons}
              <br />
              <div>{displayModalPopup}</div>
            </td>
          </tr>
        </table>
      </div>
    );
  }
}
