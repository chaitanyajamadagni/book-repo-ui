import React, { Component } from "react";
import {
  Col,
  Row,
  Button,
  Form,
  FormControl,
  Container,
  Navbar,
  Nav,
  NavDropdown
} from "react-bootstrap";
import axios from "axios";

import BookCards from "../BookCards/BookCards.js";
import AddBooks from "./AddBooks.js";
import KafkaLogs from "../Kafka/KafkaLogs.js";

export default class Books extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      response: [],
      isDeleted: false,
      isUpdated: false,
      openAddBook: false,
      showLogs: false,
      logs: []
    };
    this.isDeleted = this.isDeleted.bind(this);
    this.search = this.search.bind(this);
    this.isUpdated = this.isUpdated.bind(this);
    this.googleBook = this.googleBook.bind(this);
    this.getLogs = this.getLogs.bind(this);
  }

  componentWillMount() {
    var url = "http://localhost:8080/books";
    axios.get(url).then(response => {
      console.log(response);
      this.setState({ response: response.data });
      console.log(
        "response is->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> : ",
        response
      );
    });
  }

  isDeleted() {
    console.log("called back");
    this.setState({ isDeleted: true });
  }

  isUpdated(str) {
    console.log("here--------", str);
    var url = "http://localhost:8080/books";

    axios.get(url).then(response => {
      console.log(
        "response is->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> : ",
        response
      );
      this.setState({ response: response.data });
    });
    this.setState({ isUpdated: true });
  }

  search() {
    console.log("here--------->>>>>>>>>>>>");
    console.log(this.searchString.value);
    var url = `http://localhost:8080/books/search?key=${
      this.searchString.value
    }`;
    axios.get(url).then(response => {
      console.log(response);
      this.setState({ response: response.data });
    });
  }

  googleBook() {
    this.setState({ openAddBook: true });
  }

  getLogs() {
    console.log("logs calling---------");

    var url = `http://localhost:8080/books/logs`;
    axios.get(url).then(response => {
      console.log(response);
      console.log("response  kafka is : ", response.data);
      this.setState({ logs: response.data, showLogs: true });
    });
  }

  render() {
    var logsBookString = "";
    if (this.state.showLogs == true) {
      logsBookString = <KafkaLogs data={this.state.logs} />;
    }

    var bookCardsString = "";
    if (
      this.state.response != 0 &&
      this.state.openAddBook == false &&
      this.state.showLogs != true
    ) {
      bookCardsString = this.state.response.map(result => {
        var book = {
          id: result.id,
          title: result.title,
          author: result.authors,
          description: result.description,
          price: result.price,
          quantity: result.quantity,
          imageLinks: result.imageLinks.thumbnail
        };
        return (
          <Col>
            <BookCards
              key={result.title}
              data={book}
              deleted={this.isDeleted}
              updated={this.isUpdated}
              updateState={this.isUpdated}
              disableButtons={false}
            />
          </Col>
        );
      });
    }
    // }else {
    //   bookCardsString = (<h1>No Data available </h1>)
    // }

    var displayBookHome = "";
    if (this.state.openAddBook == true && this.state.showLogs != true) {
      displayBookHome = "";
      // call addboks component
      bookCardsString = <AddBooks />;
    } else {
      displayBookHome = (
        <div>
          <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">
              <h4> Books Inventory </h4>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto" />
              <Form inline>
                <Button variant="primary" onClick={this.getLogs}>
                  Kafka Logs
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button variant="primary" onClick={this.googleBook}>
                  Add Book
                </Button>
                &nbsp;&nbsp;&nbsp;
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                  ref={node => (this.searchString = node)}
                />
                <Button variant="outline-success" onClick={this.search}>
                  Search
                </Button>
              </Form>
            </Navbar.Collapse>
          </Navbar>
        </div>
      );
    }

    return (
      <div>
        <div>{displayBookHome}</div>
        <div>
          <Row>{bookCardsString}</Row>

          {logsBookString}
        </div>
      </div>
    );
  }
}
