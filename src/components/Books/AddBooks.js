import React, { Component } from "react";
import {
  Container,
  Col,
  Row,
  Modal,
  Button,
  FormControl,
  Form
} from "react-bootstrap";
import axios from "axios";
import BookCards from "../BookCards/BookCards.js";
import "./Books.css";

export default class AddBooks extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
    this.callGoogleApi = this.callGoogleApi.bind(this);
  }

  callGoogleApi() {
    // console.log("calling google api");
    var bookString = this.bookString.value;
    // console.log("bookString: ",bookString);
    var url = `http://localhost:8080/books/google/${bookString}`;
    axios.get(url).then(response => {
      console.log("Google Books API Data: ", response.data[0]);
      this.setState({ data: response.data[0].items });
    });
  }

  render() {
    var displayString = "";
    if (this.state.data != 0) {
      // console.log("->>>>>>>>>>>>>>>>>", this.state.data);
      displayString = this.state.data.map(result => {
        // console.log("result ->>>>>>>>>>>>>>>>>>>>>>>>>>",result.volumeInfo);
        var book = {
          id: result.volumeInfo._id,
          title: result.volumeInfo.title,
          author: result.volumeInfo.authors,
          imageLinks: result.volumeInfo.imageLinks.thumbnail,
          // author: result.volumeInfo.volumeInfoauthors,
          description: result.volumeInfo.description,
          price: result.volumeInfo.price,
          quantity: result.volumeInfo.quantity
        };
        return (
          <Col>
            <BookCards key={result._id} data={book} disableButtons={true} />
          </Col>
        );
      });
    }
    return (
      <div class="googleBooks">
        <br />
        <h4>Add Book from the Store </h4>
        <br />
        <br />
        <Form inline>
          <FormControl
            type="text"
            placeholder="Search"
            className="mr-sm-2"
            size="100"
            ref={node => (this.bookString = node)}
          />
          <Button variant="outline-success" onClick={this.callGoogleApi}>
            Search
          </Button>
        </Form>
        <Container>
          <Row>{displayString}</Row>
        </Container>
      </div>
    );
  }
}
