import {
  Navbar,
  FormControl,
  Nav,
  NavDropdown,
  Form,
  Button
} from "react-bootstrap";
import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import Books from "./components/Books/Books.js";

export default class App extends React.Component {
  constructor(props) {
    super();
    this.state = { reload: false };
    this.isUpdated = this.isUpdated.bind(this);
  }

  isUpdated(arg) {
    console.log(arg);
    this.setState({ reload: true });
  }

  render() {
    return (
      <div className="App">
        <div>
          <Navbar bg="dark" variant="dark" expand="lg">
            <Navbar.Brand href="#home">
              <img
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEA047zPiX_pvkSANgStE73yhWvqq-4CO_yPtOuIro_NQ9-_tX"
                height="80"
                width="250"
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" />
          </Navbar>
        </div>
        <div>
          <Books isUpdated={this.isUpdated} />
        </div>
      </div>
    );
  }
}
